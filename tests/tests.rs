extern crate chfl_tools;

mod centers {
    use crate::chfl_tools::centers::*;
    use chemfiles as chfl;

    #[test]
    fn center_of_geometry() {
        let mut frame = chfl::Frame::new();
        frame.set_cell(&chfl::UnitCell::new([10.0, 10.0, 10.0]));
        frame.add_atom(&chfl::Atom::new("H1"), [1.0, 2.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("H2"), [1.0, 2.0, 3.0], None);
        assert_eq!(frame.center(None, true), [1.0, 2.0, 3.0]);
        assert_eq!(frame.center(None, false), [1.0, 2.0, 3.0]);

        frame.positions_mut()[1] = [-6.0, 2.0, 3.0];
        assert_eq!(frame.center(None, true), [2.5, 2.0, 3.0]);
        assert_eq!(frame.center(None, false), [-2.5, 2.0, 3.0]);
    }

    #[test]
    fn center_of_mass() {
        let mut frame = chfl::Frame::new();
        frame.set_cell(&chfl::UnitCell::new([10.0, 10.0, 10.0]));
        frame.add_atom(&chfl::Atom::new("H1"), [1.0, 2.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("H2"), [1.0, 2.0, 3.0], None);

        frame.atom_mut(0).set_mass(1.0);
        frame.atom_mut(1).set_mass(2.0);

        assert_eq!(frame.center(Some(WeightType::Mass), true), [1.0, 2.0, 3.0]);
        assert_eq!(frame.center(Some(WeightType::Mass), false), [1.0, 2.0, 3.0]);

        frame.positions_mut()[1] = [-6.0, 2.0, 3.0];
        assert_eq!(frame.center(Some(WeightType::Mass), true), [3.0, 2.0, 3.0]);
        assert!(frame
            .center(Some(WeightType::Mass), false)
            .iter()
            .zip(&[-3.66666, 2.0, 3.0])
            .all(|(a, b)| (a - b).abs() < 1e-5));
    }

    #[test]
    fn center_of_charge() {
        let mut frame = chfl::Frame::new();
        frame.set_cell(&chfl::UnitCell::new([10.0, 10.0, 10.0]));
        frame.add_atom(&chfl::Atom::new("H1"), [1.0, 2.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("H2"), [1.0, 2.0, 3.0], None);

        frame.atom_mut(0).set_charge(1.0);
        frame.atom_mut(1).set_charge(2.0);

        assert_eq!(
            frame.center(Some(WeightType::Charge), true),
            [1.0, 2.0, 3.0]
        );
        assert_eq!(
            frame.center(Some(WeightType::Charge), false),
            [1.0, 2.0, 3.0]
        );

        frame.positions_mut()[1] = [-6.0, 2.0, 3.0];
        assert_eq!(
            frame.center(Some(WeightType::Charge), true),
            [3.0, 2.0, 3.0]
        );
        assert!(frame
            .center(Some(WeightType::Charge), false)
            .iter()
            .zip(&[-3.66666, 2.0, 3.0])
            .all(|(a, b)| (a - b).abs() < 1e-5));
    }

    #[test]
    fn atoms_center_of_geometry() {
        let mut frame = chfl::Frame::new();
        frame.set_cell(&chfl::UnitCell::new([10.0, 10.0, 10.0]));
        frame.add_atom(&chfl::Atom::new("H1"), [1.0, 2.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("C1"), [3.0, 4.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("C2"), [4.0, 3.0, 4.0], None);
        frame.add_atom(&chfl::Atom::new("H2"), [1.0, 2.0, 3.0], None);
        let atom_idx = vec![0, 3];
        assert_eq!(frame.center_atoms(&atom_idx, None, true), [1.0, 2.0, 3.0]);
        assert_eq!(frame.center_atoms(&atom_idx, None, false), [1.0, 2.0, 3.0]);

        frame.positions_mut()[3] = [-6.0, 2.0, 3.0];
        assert_eq!(frame.center_atoms(&atom_idx, None, true), [2.5, 2.0, 3.0]);
        assert_eq!(frame.center_atoms(&atom_idx, None, false), [-2.5, 2.0, 3.0]);
    }

    #[test]
    fn atoms_center_of_mass() {
        let mut frame = chfl::Frame::new();
        frame.set_cell(&chfl::UnitCell::new([10.0, 10.0, 10.0]));
        frame.add_atom(&chfl::Atom::new("H1"), [1.0, 2.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("C1"), [3.0, 4.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("C2"), [4.0, 3.0, 4.0], None);
        frame.add_atom(&chfl::Atom::new("H2"), [1.0, 2.0, 3.0], None);

        frame.atom_mut(0).set_mass(1.0);
        frame.atom_mut(1).set_mass(10.0);
        frame.atom_mut(2).set_mass(20.0);
        frame.atom_mut(3).set_mass(2.0);

        let atom_idx = vec![0, 3];
        assert_eq!(
            frame.center_atoms(&atom_idx, Some(WeightType::Mass), true),
            [1.0, 2.0, 3.0]
        );
        assert_eq!(
            frame.center_atoms(&atom_idx, Some(WeightType::Mass), false),
            [1.0, 2.0, 3.0]
        );

        frame.positions_mut()[3] = [-6.0, 2.0, 3.0];
        assert_eq!(
            frame.center_atoms(&atom_idx, Some(WeightType::Mass), true),
            [3.0, 2.0, 3.0]
        );
        assert!(frame
            .center_atoms(&atom_idx, Some(WeightType::Mass), false)
            .iter()
            .zip(&[-3.66666, 2.0, 3.0])
            .all(|(a, b)| (a - b).abs() < 1e-5));
    }

    #[test]
    fn atoms_center_of_charge() {
        let mut frame = chfl::Frame::new();
        frame.set_cell(&chfl::UnitCell::new([10.0, 10.0, 10.0]));
        frame.add_atom(&chfl::Atom::new("H1"), [1.0, 2.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("C1"), [3.0, 4.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("C2"), [4.0, 3.0, 4.0], None);
        frame.add_atom(&chfl::Atom::new("H2"), [1.0, 2.0, 3.0], None);

        frame.atom_mut(0).set_charge(1.0);
        frame.atom_mut(1).set_charge(10.0);
        frame.atom_mut(2).set_charge(20.0);
        frame.atom_mut(3).set_charge(2.0);

        let atom_idx = vec![0, 3];
        assert_eq!(
            frame.center_atoms(&atom_idx, Some(WeightType::Charge), true),
            [1.0, 2.0, 3.0]
        );
        assert_eq!(
            frame.center_atoms(&atom_idx, Some(WeightType::Charge), false),
            [1.0, 2.0, 3.0]
        );

        frame.positions_mut()[3] = [-6.0, 2.0, 3.0];
        assert_eq!(
            frame.center_atoms(&atom_idx, Some(WeightType::Charge), true),
            [3.0, 2.0, 3.0]
        );
        assert!(frame
            .center_atoms(&atom_idx, Some(WeightType::Charge), false)
            .iter()
            .zip(&[-3.66666, 2.0, 3.0])
            .all(|(a, b)| (a - b).abs() < 1e-5));
    }

    #[test]
    fn atoms_center_of_geometry_pbc() {
        let mut frame = chfl::Frame::new();
        frame.set_cell(&chfl::UnitCell::new([10.0; 3]));
        frame.add_atom(&chfl::Atom::new("H1"), [-4.0; 3], None);
        frame.add_atom(&chfl::Atom::new("C1"), [3.0, 4.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("H2"), [4.0; 3], None);
        let atom_idx = vec![0, 2];
        assert!(frame
            .center_atoms_pbc(&atom_idx, None)
            .iter()
            .zip(&[5.0, 5.0, 5.0])
            .all(|(a, b)| (a - b).abs() < 1e-5));
    }

    #[test]
    fn atoms_center_of_mass_pbc() {
        let mut frame = chfl::Frame::new();
        frame.set_cell(&chfl::UnitCell::new([10.0, 10.0, 10.0]));
        frame.add_atom(&chfl::Atom::new("H1"), [-1.0, 4.0, 6.0], None);
        frame.add_atom(&chfl::Atom::new("C1"), [3.0, 4.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("H2"), [1.0, 3.0, 4.0], None);

        frame.atom_mut(0).set_mass(1.0);
        frame.atom_mut(2).set_mass(2.0);

        let atom_idx = vec![0, 2];
        assert!(frame
            .center_atoms_pbc(&atom_idx, Some(WeightType::Mass))
            .iter()
            .zip(&[0.33, 3.33, 4.66])
            .all(|(a, b)| (a - b).abs() < 1e-1));
    }

    #[test]
    fn atoms_center_of_charge_pbc() {
        let mut frame = chfl::Frame::new();
        frame.set_cell(&chfl::UnitCell::new([10.0, 10.0, 10.0]));
        frame.add_atom(&chfl::Atom::new("H1"), [-1.0, 4.0, 6.0], None);
        frame.add_atom(&chfl::Atom::new("C1"), [3.0, 4.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("H2"), [1.0, 3.0, 4.0], None);

        frame.atom_mut(0).set_charge(1.0);
        frame.atom_mut(2).set_charge(2.0);

        let atom_idx = vec![0, 2];
        assert!(frame
            .center_atoms_pbc(&atom_idx, Some(WeightType::Charge))
            .iter()
            .zip(&[0.33, 3.33, 4.66])
            .all(|(a, b)| (a - b).abs() < 1e-1));
    }
}

mod datatsets {
    mod welfords_dataset {
        use crate::chfl_tools::datasets::WOADataset;

        #[test]
        fn construct_and_update() {
            let mut ds = WOADataset::new();

            assert!(ds.len() == 0);
            assert!(ds.is_empty());

            ds.update(1.0);
            ds.update(2.0);
            ds.update(3.0);
            ds.update(-4.0);

            assert!(ds.len() == 4);
            assert!(!ds.is_empty());

            assert!((ds.calc_mean().unwrap() - 0.5).abs() < 1e-5);
            assert!((ds.calc_var().unwrap() - 7.25).abs() < 1e-5);
            assert!((ds.calc_std().unwrap() - 2.69258).abs() < 1e-5);

            assert_eq!(
                format!("{:?}", ds),
                format!(
                    "WOADataset {{ mean: Some(0.5), std: {:?}, size: 4 }}",
                    ds.calc_std()
                )
            );
        }

        #[test]
        fn join() {
            let test_data = [
                446.272, 461.544, 499.271, 163.178, -381.884, -71.708, -292.363, -312.385, 105.681,
                -419.94, 38.295, -342.486, -323.592, 318.1, -352.765, -334.05, 135.583, 479.536,
                -441.502, -329.117, 79.269, 407.571, -326.561,
            ];
            let ref_mean = -34.524043478260865;
            let ref_std = 334.5039596980786;

            let mut ds = WOADataset::new();
            for v in &test_data[..7] {
                ds.update(*v);
            }
            let mut ds2 = WOADataset::new();
            ds2.update(test_data[7]);
            let mut ds3 = WOADataset::new();
            for v in &test_data[8..] {
                ds3.update(*v);
            }

            ds.join(ds2);
            ds.join(ds3);
            ds.join(WOADataset::new());

            assert_eq!(ds.len(), test_data.len());
            assert!((ds.calc_mean().unwrap() - ref_mean).abs() < 1e-5);
            assert!((ds.calc_std().unwrap() - ref_std).abs() < 1e-5);

            let mut ds_empty = WOADataset::new();
            ds_empty.join(ds);
            assert_eq!(ds_empty.len(), test_data.len());
            assert!((ds_empty.calc_mean().unwrap() - ref_mean).abs() < 1e-5);
            assert!((ds_empty.calc_std().unwrap() - ref_std).abs() < 1e-5);
        }
    }

    mod binned_data {
        use crate::chfl_tools::datasets::BinnedData;

        fn test_binned_data<const DYN: bool>(binned_data: &mut BinnedData<DYN>, origin: f64) {
            binned_data.insert(0.0 + origin, 0.0);
            binned_data.insert(0.05 + origin, 0.0);
            binned_data.insert(-0.05 + origin, -1.0);
            binned_data.insert(0.35 + origin, 3.0);
            binned_data.insert(0.39 + origin, 3.0);
            binned_data.insert(-0.35 + origin, -4.0);
            binned_data.insert(-0.39 + origin, -4.0);

            let values: Vec<_> = (0..binned_data.n_bins())
                .map(|i| binned_data.value(i))
                .collect();
            let counts: Vec<_> = (0..binned_data.n_bins())
                .map(|i| binned_data.data()[i].len())
                .collect();
            let means: Vec<_> = (0..binned_data.n_bins())
                .map(|i| binned_data.data()[i].calc_mean().unwrap_or(0.0))
                .collect();
            let stds: Vec<_> = (0..binned_data.n_bins())
                .map(|i| binned_data.data()[i].calc_std().unwrap_or(0.0))
                .collect();

            assert_eq!(binned_data.n_bins(), 8);

            let ref_values = [-0.35, -0.25, -0.15, -0.05, 0.05, 0.15, 0.25, 0.35];
            for (val, ref_val) in values.iter().zip(&ref_values) {
                dbg!(val + origin, ref_val);
                assert!((val - ref_val - origin).abs() < 1e-5);
            }

            let ref_counts = [2, 0, 0, 1, 2, 0, 0, 2];
            assert_eq!(counts, ref_counts);

            let ref_means = [-4.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, 3.0];
            for (val, ref_val) in means.iter().zip(&ref_means) {
                assert!((val - ref_val).abs() < 1e-5);
            }

            for val in stds.into_iter() {
                assert!(val < 1e-5);
            }
        }

        #[test]
        fn fixed_with_around_zero() {
            let mut binned_data = BinnedData::<false>::new(8, -0.4, 0.4);
            test_binned_data(&mut binned_data, 0.0);
        }

        #[test]
        fn fixed_with_around_origin() {
            let mut binned_data = BinnedData::<false>::new(8, 10.1, 10.9);
            test_binned_data(&mut binned_data, 10.5);
        }

        #[test]
        fn dynamic_width_around_zero() {
            let mut binned_data = BinnedData::<true>::new(0.1, None);
            test_binned_data(&mut binned_data, 0.0);
        }

        #[test]
        fn dynamic_width_around_origin() {
            let mut binned_data = BinnedData::<true>::new(0.1, Some(10.5));
            test_binned_data(&mut binned_data, 10.5);
        }
    }
}

mod filters {
    use crate::chfl_tools::filters::*;
    use chemfiles as chfl;
    use regex::Regex;

    #[test]
    fn name() {
        let mut frame = chfl::Frame::new();

        frame.add_atom(&chfl::Atom::new("H1"), [1.0, 2.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("H2"), [1.0, 2.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("H3"), [1.0, 2.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("H10"), [1.0, 2.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("Hf1"), [1.0, 2.0, 3.0], None);

        let re = Regex::new(r"^H\d+$").unwrap();
        let result = frame.filter_name(&re);
        assert!(result.len() == 4)
    }

    #[test]
    fn atomic_type() {
        let mut frame = chfl::Frame::new();

        frame.add_atom(&chfl::Atom::new("H1"), [1.0, 2.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("H2"), [1.0, 2.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("H3"), [1.0, 2.0, 3.0], None);
        frame.atom_mut(2).set_atomic_type("C");
        frame.add_atom(&chfl::Atom::new("H10"), [1.0, 2.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("Hf1"), [1.0, 2.0, 3.0], None);

        let re = Regex::new(r"^H\d+$").unwrap();
        let result = frame.filter_type(&re);
        assert!(result.len() == 3)
    }

    #[test]
    fn property() {
        let mut frame = chfl::Frame::new();

        frame.add_atom(&chfl::Atom::new("H1"), [1.0, 2.0, 3.0], None);
        frame.atom_mut(0).set("test", "X");
        frame.add_atom(&chfl::Atom::new("C1"), [1.0, 2.0, 3.0], None);
        frame.atom_mut(1).set("test", "Y");
        frame.add_atom(&chfl::Atom::new("H3"), [1.0, 2.0, 3.0], None);
        frame.atom_mut(2).set("not_test", "A");
        frame.add_atom(&chfl::Atom::new("H10"), [1.0, 2.0, 3.0], None);
        frame.atom_mut(3).set("test", "D");
        frame.add_atom(&chfl::Atom::new("Hf1"), [1.0, 2.0, 3.0], None);
        frame.atom_mut(4).set("test", true);
        frame.add_atom(&chfl::Atom::new("Hf2"), [1.0, 2.0, 3.0], None);

        let re = Regex::new(r"^[X|Y]$").unwrap();
        let result = frame.filter_property("test", &re);
        assert!(result.len() == 2)
    }

    #[test]
    fn unique_residues() {
        let mut frame = chfl::Frame::new();
        frame.add_atom(&chfl::Atom::new("H1"), [1.0, 2.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("H2"), [1.0, 2.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("H3"), [1.0, 2.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("C1"), [1.0, 2.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("C2"), [1.0, 2.0, 3.0], None);
        frame.add_atom(&chfl::Atom::new("C3"), [1.0, 2.0, 3.0], None);
        let mut residue = chfl::Residue::with_id("test1", 0);
        residue.add_atom(0);
        residue.add_atom(1);
        residue.add_atom(2);
        frame.add_residue(&residue).unwrap();
        residue = chfl::Residue::with_id("test2", 1);
        residue.add_atom(3);
        residue.add_atom(4);
        residue.add_atom(5);
        frame.add_residue(&residue).unwrap();

        let result = frame.unique_residues([0, 2, 4].to_vec());
        assert!(result.len() == 2);
    }
}

mod ranges {
    use crate::chfl_tools::ranges::LogRange;

    #[test]
    fn log_range() {
        let mut lr = LogRange::new(0, 100, 10, false).unwrap();
        assert!(lr.collect::<Vec<usize>>() == vec![0, 1, 10]);

        lr = LogRange::new(0, 100, 10, true).unwrap();
        assert!(lr.collect::<Vec<usize>>() == vec![0, 1, 10, 100]);

        lr = LogRange::new(0, 130, 10, true).unwrap();
        assert!(lr.collect::<Vec<usize>>() == vec![0, 1, 10, 100, 130]);

        lr = LogRange::new(3, 100, 10, true).unwrap();
        assert!(lr.collect::<Vec<usize>>() == vec![3, 30, 100]);

        lr = LogRange::new(0, 100, 2, false).unwrap();
        assert!(lr.collect::<Vec<usize>>() == vec![0, 1, 2, 4, 8, 16, 32, 64]);
    }
}
