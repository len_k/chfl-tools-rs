#![cfg(feature = "config")]

use chemfiles::{Atom, Topology};

extern crate chfl_tools;
use crate::chfl_tools::config::*;

#[test]
fn config() {
    let mut top = Topology::new();
    top.add_atom(&Atom::new("X"));
    top.add_atom(&Atom::new("Hw"));
    top.add_atom(&Atom::new("Ow"));
    top.add_atom(&Atom::new("Hw"));
    top.add_atom(&Atom::new("CH3"));

    load_config(&mut top, "tests/config/configuration.toml").unwrap();

    assert!((top.atom(0).mass() - 100.99).abs() < 1e-3);

    assert_eq!(top.atom(1).atomic_type(), "H");
    assert!((top.atom(1).mass() - 1.23).abs() < 1e-3);

    assert_eq!(top.atom(2).name(), "Oxygen");
    assert!((top.atom(2).charge() - -2.34).abs() < 1e-3);

    assert_eq!(top.atom(3).atomic_type(), "H");
    assert!((top.atom(3).mass() - 1.23).abs() < 1e-3);

    assert_eq!(top.atom(4).name(), "Methyl");
}
