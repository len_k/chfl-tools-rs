use chemfiles as chfl;
use itertools::Itertools;
use regex::Regex;

pub trait AtomFilter {
    fn filter_name(&self, filter: &Regex) -> Vec<usize>;
    fn filter_type(&self, filter: &Regex) -> Vec<usize>;
    fn filter_property(&self, prop_name: &str, filter: &Regex) -> Vec<usize>;
}

impl AtomFilter for chfl::Frame {
    fn filter_name(&self, filter: &Regex) -> Vec<usize> {
        (0..self.size())
            .filter(|&atom_idx| filter.is_match(&self.atom(atom_idx).name()))
            .collect()
    }

    fn filter_type(&self, filter: &Regex) -> Vec<usize> {
        (0..self.size())
            .filter(|&atom_idx| filter.is_match(&self.atom(atom_idx).atomic_type()))
            .collect()
    }

    fn filter_property(&self, name: &str, filter: &Regex) -> Vec<usize> {
        (0..self.size())
            .filter(|&atom_idx| {
                if let Some(chfl::Property::String(pstr)) = self.atom(atom_idx).get(name) {
                    filter.is_match(&pstr)
                } else {
                    false
                }
            })
            .collect()
    }
}

pub trait ResidueFilter {
    fn unique_residues(&self, atom_idx: impl std::iter::IntoIterator<Item = usize>) -> Vec<i64>;
}

impl ResidueFilter for chfl::Frame {
    fn unique_residues(&self, atom_idx: impl std::iter::IntoIterator<Item = usize>) -> Vec<i64> {
        let topology = self.topology();
        atom_idx
            .into_iter()
            .filter_map(|atom_idx| topology.residue_for_atom(atom_idx))
            .filter_map(|res| res.id())
            .unique()
            .collect()
    }
}
