use chemfiles as chfl;
use serde::Deserialize;
use std::collections::HashMap;
use std::error::Error;
use std::fs;
use std::path::Path;
use toml::from_str;

#[derive(Deserialize, Debug)]
struct AtomData {
    atomic_type: Option<String>,
    name: Option<String>,
    mass: Option<f64>,
    charge: Option<f64>,
}

pub fn load_config<P: AsRef<Path>>(
    topol: &mut chfl::Topology,
    path: P,
) -> Result<(), Box<dyn Error>> {
    let atoms_str = fs::read_to_string(path)?;

    let atoms_table: HashMap<String, HashMap<String, AtomData>> = from_str(&atoms_str)?;
    if let Some(type_table) = atoms_table.get("types") {
        for i in 0..topol.size() {
            let mut atom = topol.atom_mut(i);
            if let Some(atom_data) = type_table.get(&atom.atomic_type()) {
                atom_data
                    .atomic_type
                    .iter()
                    .for_each(|at| atom.set_atomic_type(&**at));
                atom_data.name.iter().for_each(|nm| atom.set_name(&**nm));
                atom_data.mass.iter().for_each(|m| atom.set_mass(*m));
                atom_data.charge.iter().for_each(|q| atom.set_charge(*q));
            }
        }
    } else {
        return Err("No 'types' entry found".to_string().into());
    }
    Ok(())
}
