use chemfiles as chfl;

#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum WeightType {
    Mass,
    Charge,
}

pub trait Center {
    fn center(&self, weight_type: Option<WeightType>, wrap: bool) -> [f64; 3];
    fn center_atoms(
        &self,
        atom_idx: &[usize],
        weight_type: Option<WeightType>,
        wrap: bool,
    ) -> [f64; 3];
    fn center_atoms_pbc(&self, atom_idx: &[usize], weight_type: Option<WeightType>) -> [f64; 3];
}

impl Center for chfl::Frame {
    fn center(&self, weight_type: Option<WeightType>, wrap: bool) -> [f64; 3] {
        let range: Vec<_> = (0..self.size()).collect();
        self.center_atoms(&range, weight_type, wrap)
    }

    fn center_atoms(
        &self,
        atom_idx: &[usize],
        weight_type: Option<WeightType>,
        wrap: bool,
    ) -> [f64; 3] {
        let weight_sum;
        let mut center_pos = if let Some(weight_type) = weight_type {
            let weights: Vec<_> = atom_idx
                .iter()
                .map(|&i| {
                    let atom = &self.atom(i);
                    match weight_type {
                        WeightType::Mass => atom.mass(),
                        WeightType::Charge => atom.charge(),
                    }
                })
                .collect();
            weight_sum = weights.iter().sum();
            atom_idx
                .iter()
                .zip(&weights)
                .map(|(&i, w)| {
                    let mut pos = self.positions()[i];
                    if wrap {
                        self.cell().wrap(&mut pos);
                    }
                    [w * pos[0], w * pos[1], w * pos[2]]
                })
                .fold([0.0; 3], |acc, wp| acc.add(&wp))
        } else {
            weight_sum = atom_idx.len() as f64;
            atom_idx
                .iter()
                .map(|&i| {
                    let mut pos = self.positions()[i];
                    if wrap {
                        self.cell().wrap(&mut pos);
                    }
                    pos
                })
                .fold([0.0; 3], |acc, wp| acc.add(&wp))
        };

        center_pos[0] /= weight_sum;
        center_pos[1] /= weight_sum;
        center_pos[2] /= weight_sum;

        center_pos
    }

    fn center_atoms_pbc(&self, atom_idx: &[usize], weight_type: Option<WeightType>) -> [f64; 3] {
        /* For details see https://doi.org/10.1080/2151237X.2008.10129266 */
        let cartesian_lengths = {
            let cell_mat = &self.cell().matrix();
            [cell_mat[0][0], cell_mat[1][1], cell_mat[2][2]]
        };
        let weight_sum;
        let mut center_mapped_pos = if let Some(weight_type) = weight_type {
            /* ! Weighting produces significant rounding errors */
            let weights: Vec<_> = atom_idx
                .iter()
                .map(|&i| {
                    let atom = &self.atom(i);
                    match weight_type {
                        WeightType::Mass => atom.mass(),
                        WeightType::Charge => atom.charge(),
                    }
                })
                .collect();
            weight_sum = weights.iter().sum();
            atom_idx
                .iter()
                .zip(&weights)
                .map(|(&i, w)| {
                    let mut pos = self.positions()[i];
                    self.cell().wrap(&mut pos);
                    let theta_i = [
                        pos[0] / cartesian_lengths[0] * std::f64::consts::TAU,
                        pos[1] / cartesian_lengths[1] * std::f64::consts::TAU,
                        pos[2] / cartesian_lengths[2] * std::f64::consts::TAU,
                    ];
                    let weighted_xi_i = [
                        theta_i[0].cos() * w,
                        theta_i[1].cos() * w,
                        theta_i[2].cos() * w,
                    ];
                    let weighted_zeta_i = [
                        theta_i[0].sin() * w,
                        theta_i[1].sin() * w,
                        theta_i[2].sin() * w,
                    ];
                    [weighted_xi_i, weighted_zeta_i]
                })
                .fold([[0.0; 3]; 2], |acc, wp| {
                    [acc[0].add(&wp[0]), acc[1].add(&wp[1])]
                })
        } else {
            weight_sum = atom_idx.len() as f64;
            atom_idx
                .iter()
                .map(|&i| {
                    let mut pos = self.positions()[i];
                    self.cell().wrap(&mut pos);
                    let theta_i = [
                        pos[0] / cartesian_lengths[0] * std::f64::consts::TAU,
                        pos[1] / cartesian_lengths[1] * std::f64::consts::TAU,
                        pos[2] / cartesian_lengths[2] * std::f64::consts::TAU,
                    ];
                    let weighted_xi_i = [theta_i[0].cos(), theta_i[1].cos(), theta_i[2].cos()];
                    let weighted_zeta_i = [theta_i[0].sin(), theta_i[1].sin(), theta_i[2].sin()];
                    [weighted_xi_i, weighted_zeta_i]
                })
                .fold([[0.0; 3]; 2], |acc, wp| {
                    [acc[0].add(&wp[0]), acc[1].add(&wp[1])]
                })
        };

        center_mapped_pos[0][0] /= -weight_sum;
        center_mapped_pos[0][1] /= -weight_sum;
        center_mapped_pos[0][2] /= -weight_sum;

        center_mapped_pos[1][0] /= -weight_sum;
        center_mapped_pos[1][1] /= -weight_sum;
        center_mapped_pos[1][2] /= -weight_sum;

        let minus_xi_bar = &center_mapped_pos[0];
        let minus_zeta_bar = &center_mapped_pos[1];

        let theta_bar = [
            minus_zeta_bar[0].atan2(minus_xi_bar[0]) + std::f64::consts::PI,
            minus_zeta_bar[1].atan2(minus_xi_bar[1]) + std::f64::consts::PI,
            minus_zeta_bar[2].atan2(minus_xi_bar[2]) + std::f64::consts::PI,
        ];

        [
            cartesian_lengths[0] * theta_bar[0] / std::f64::consts::TAU,
            cartesian_lengths[1] * theta_bar[1] / std::f64::consts::TAU,
            cartesian_lengths[2] * theta_bar[2] / std::f64::consts::TAU,
        ]
    }
}

trait Array3Add {
    fn add(&self, other: &Self) -> Self;
}

impl Array3Add for [f64; 3] {
    fn add(&self, other: &Self) -> Self {
        [self[0] + other[0], self[1] + other[1], self[2] + other[2]]
    }
}
