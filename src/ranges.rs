#[derive(Clone, Debug)]
pub struct LogRange {
    end: usize,
    base: usize,
    endpoint: bool,
    state: usize,
}

impl LogRange {
    pub fn new(start: usize, end: usize, base: usize, endpoint: bool) -> Result<Self, String> {
        if start > end {
            return Err(format!(
                "Start ({}), must be less or equal than end ({}).",
                start, end
            ));
        }
        Ok(LogRange {
            end,
            base,
            endpoint,
            state: start,
        })
    }
}

impl Iterator for LogRange {
    type Item = usize;
    fn next(&mut self) -> Option<Self::Item> {
        let rs = if self.state < self.end {
            Some(self.state)
        } else if self.endpoint {
            self.endpoint = false;
            Some(self.end)
        } else {
            None
        };

        if self.state == 0 {
            self.state = 1;
        } else {
            self.state *= self.base;
        }

        rs
    }
}
