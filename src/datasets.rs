use csv::Writer;
use num_traits::{cast, Float, NumCast};
use num_traits::{One, Zero};

// https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Welford's_online_algorithm
#[allow(clippy::upper_case_acronyms)]
#[derive(Clone, Default)]
pub struct WOADataset {
    count: usize,
    cumsum: f64,
    m2: f64,
}

impl WOADataset {
    pub fn new() -> Self {
        WOADataset::default()
    }

    pub fn update(&mut self, val: f64) {
        let mean_before = self.calc_mean().unwrap_or(0.0);

        self.count += 1;
        self.cumsum += val;

        let mean_after = self
            .calc_mean()
            .expect("Dataset should not be empty after update");

        let delta = val - mean_before;
        let delta2 = val - mean_after;
        self.m2 += delta * delta2;
    }

    pub fn len(&self) -> usize {
        self.count
    }

    pub fn is_empty(&self) -> bool {
        self.count == 0
    }

    pub fn calc_mean(&self) -> Option<f64> {
        if self.is_empty() {
            None
        } else {
            Some(self.cumsum / self.count as f64)
        }
    }

    pub fn calc_var(&self) -> Option<f64> {
        if self.is_empty() {
            None
        } else {
            Some(self.m2 / self.count as f64)
        }
    }

    pub fn calc_std(&self) -> Option<f64> {
        Some(self.calc_var()?.sqrt())
    }

    pub fn join(&mut self, other: Self) {
        let delta_sq =
            (other.calc_mean().unwrap_or(0.0) - self.calc_mean().unwrap_or(0.0)).powf(2.0);
        let n = self.count + other.count;
        self.m2 += other.m2 + delta_sq * (self.count * other.count) as f64 / n as f64;
        self.count = n;
        self.cumsum += other.cumsum;
    }
}

impl std::fmt::Debug for WOADataset {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("WOADataset")
            .field("mean", &self.calc_mean())
            .field("std", &self.calc_std())
            .field("size", &self.len())
            .finish()
    }
}

#[derive(Clone, Debug)]
pub struct Histogram<T, U> {
    n_bins: usize,
    min: U,
    max: U,
    width: U,
    data: Vec<T>,
}

impl<T, U> Histogram<T, U>
where
    T: std::ops::AddAssign + std::ops::MulAssign + std::fmt::Display + Clone + Copy + Zero + One,
    U: Float + std::fmt::Display,
{
    pub fn new(n_bins: usize, min: U, max: U) -> Self {
        assert!(min < max, "Invalid histogram range");
        Self {
            n_bins,
            min,
            max,
            width: max - min,
            data: vec![T::zero(); n_bins],
        }
    }

    pub fn insert(&mut self, val: U) {
        self.insert_weighted(val, T::one());
    }

    pub fn insert_weighted(&mut self, val: U, weight: T) {
        assert!(val >= self.min);
        assert!(val < self.max);
        let calc_bin = || -> Option<usize> {
            let n_bins = NumCast::from(self.n_bins)?;
            let bin = NumCast::from(((val - self.min) / self.width * n_bins).floor())?;
            Some(bin)
        };
        match calc_bin() {
            Some(bin) => self.data[bin] += weight,
            None => panic!("Numeric conversion error"),
        }
    }

    pub fn value(&self, bin: usize) -> U {
        assert!(bin < self.n_bins);
        let calc_value = || -> Option<U> {
            let bin = cast::cast::<_, U>(bin)?;
            let n_bins = NumCast::from(self.n_bins)?;
            let half_float = NumCast::from(0.5)?;
            Some(self.min + (bin + half_float) * self.width / n_bins)
        };
        match calc_value() {
            Some(v) => v,
            None => panic!("Numeric conversion error"),
        }
    }

    pub fn scale_data(&mut self, scale: T) {
        self.data.iter_mut().for_each(|d| *d *= scale);
    }

    pub fn to_csv(&self, path: &str) -> Result<(), Box<dyn std::error::Error>> {
        let mut wtr = Writer::from_path(path)?;
        wtr.write_record(["bin", "value", "count"])?;
        for (bin, count) in self.data.iter().enumerate() {
            wtr.write_record(&[
                format!("{}", bin),
                format!("{}", self.value(bin)),
                format!("{}", count),
            ])?;
        }
        wtr.flush()?;
        Ok(())
    }

    pub fn n_bins(&self) -> usize {
        self.n_bins
    }

    pub fn data(&self) -> &[T] {
        &self.data
    }

    pub fn clear(&mut self) {
        self.data.clear();
        self.data.resize(self.n_bins, T::zero());
    }
}

#[derive(Debug)]
pub struct BinnedData<const DYN: bool> {
    origin: f64,
    bin_width: f64,
    min: f64,
    max: f64,
    data: Vec<WOADataset>,
    idx_offset: usize,
}

impl<const DYN: bool> BinnedData<DYN> {
    #[allow(clippy::cast_precision_loss)]
    pub fn value(&self, bin: usize) -> f64 {
        assert!(bin < self.n_bins());
        self.min + ((bin as isize - self.idx_offset as isize) as f64 + 0.5) * self.bin_width
    }

    pub fn to_csv(&self, path: &str) -> Result<(), Box<dyn std::error::Error>> {
        let mut wtr = Writer::from_path(path)?;
        wtr.write_record(["bin", "value", "mean", "std", "count"])?;
        for (bin, dataset) in self.data.iter().enumerate() {
            wtr.write_record(&[
                format!("{}", bin),
                format!("{}", self.value(bin)),
                format!("{}", dataset.calc_mean().unwrap()),
                format!("{}", dataset.calc_std().unwrap()),
                format!("{}", dataset.len()),
            ])?;
        }
        wtr.flush()?;
        Ok(())
    }

    pub fn n_bins(&self) -> usize {
        self.data.len()
    }

    pub fn data(&self) -> &[WOADataset] {
        &self.data
    }

    pub fn insert(&mut self, x: f64, val: f64) {
        #[derive(PartialEq)]
        enum InsertNewElements {
            Left,
            Right,
            None,
        }

        let bin = if DYN {
            // shift value to origin
            // used only for bin calculation
            let x = x - self.origin;
            let bidirectional_bin = (x / self.bin_width).floor() as isize;
            let shifted_bin = bidirectional_bin + self.idx_offset as isize;
            let insert_mode = if shifted_bin < 0 {
                InsertNewElements::Left
            } else if shifted_bin >= self.n_bins() as isize {
                InsertNewElements::Right
            } else {
                InsertNewElements::None
            };
            let n_new_bins = match insert_mode {
                InsertNewElements::Left => shifted_bin.unsigned_abs(),
                InsertNewElements::Right => (shifted_bin - (self.n_bins() - 1) as isize) as usize,
                _ => 0,
            };
            self.data
                .resize(self.data.len() + n_new_bins, WOADataset::new());
            if insert_mode == InsertNewElements::Left {
                self.idx_offset += n_new_bins;
                self.data.rotate_right(n_new_bins);
            }
            (bidirectional_bin + self.idx_offset as isize) as usize
        } else {
            assert!(x >= self.min);
            assert!(x < self.max);
            let width = self.max - self.min;
            #[allow(
                clippy::cast_possible_truncation,
                clippy::cast_precision_loss,
                clippy::cast_sign_loss
            )]
            let bin = ((x - self.min) / width * self.n_bins() as f64).floor() as usize;
            bin
        };
        self.data[bin].update(val);
    }
}

impl BinnedData<false> {
    pub fn new(n_bins: usize, min: f64, max: f64) -> Self {
        assert!(min < max, "Invalid range");
        let width = max - min;
        Self {
            origin: width / 2.0,
            bin_width: width / n_bins as f64,
            min,
            max,
            data: vec![WOADataset::new(); n_bins],
            idx_offset: 0,
        }
    }
}

impl BinnedData<true> {
    pub fn new(bin_width: f64, origin: Option<f64>) -> Self {
        assert!(bin_width > 0.0, "Invalid bin width");
        let n_bins = 1;
        let origin = origin.unwrap_or(0.0);
        let min = origin;
        let max = bin_width;
        Self {
            origin,
            bin_width,
            min,
            max,
            data: vec![WOADataset::new(); n_bins],
            idx_offset: 0,
        }
    }
}
